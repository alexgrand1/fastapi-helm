## How Deploy to minikube

1. Start minikube 
    ```bash
    minikube start
    ```
2. Build your image in minikube scope
   ```bash
   docker build -f docker/Dockerfile . -t localhost:5000/fastapi-app
   ```
3. Run minikube registry ([docs])
   ```bash
   minikube addons enable registry &&
   docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"
   ```
4. Push your image to the minikube registry
   ```bash
   docker push localhost:5000/fastapi-app
   ```
5. Verify that image exists in the minikube registry
   ```bash
   curl -X GET localhost:5000/v2/_catalog
   ```
6. Run you helm install
   ```bash
   helm install fastapi-app helm
   ```
7. Run minikube dashboard
   ```bash
   minikube dashboard
   ```
8. Get url
   ```bash
   minikube service list
   ```
9. *Optional* add ingress
   * enable nginx ingress addon:
   ```bash
   minikube addons enable ingress
   ```
   * validate that app is available via hostname:
   ```bash
    curl --resolve "fastapi-app.local:80:$(minikube ip)" -i http://fastapi-app.local
   ```
   * if you want to see service via browser get `minikube ip` and add it to the `/etc/hosts`
   ```bash
   <minikube_ip> fastapi-app.local
   ```
   * Validate in browser:
   ```bash
   http://fastapi-app.local
   ```

[docs]:https://minikube.sigs.k8s.io/docs/handbook/registry/#enabling-insecure-registries